#!/bin/bash
# Moncii's system setup script for Arch Linux.

# System Update.
sudo pacman -Syu
sudo pacman -S git

# Paru
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
echo
echo "--> SELECT RUST FOR RUST INSTALL. <--"
echo
cd paru && makepkg -si

# Configs
git clone https://gitlab.com/Moncii/config-files.git
doas cp config-files/pacman.conf /etc/pacman.conf
doas cp config-files/paru.conf /etc/paru.conf

# System
sudo pacman -S i3-gaps i3status rofi power-profiles-daemon sof-firmware network-manager-applet\
     polybar dunst kitty fish neovim dust thunar libreoffice-fresh gimp papirus-icon-theme\
     qutebrowser ttf-jetbrains-mono-nerd opendoas lsd bottom xorg-xrandr xorg-xset
paru -S pfetch picom-arian8j2-git betterlockscreen

cp -r config-files/* ~/.config/

# Wallpapers
git clone https://gitlab.com/Moncii/wallpapers.git
cp -r wallpapers ~/Pictures/Wallpapers/

# Cleanup
sudo pacman -Rns rust
rm -rf ~/.cargo

# Reboot
sudo reboot
